# gas station project
package gasStation;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {

        int fuelTankMax = 1000;
        int fuelTankMin = 0;
        int fuelAmount = 1000;


        fuelPump n1 = new fuelPump(0,fuelAmount,fuelTankMin,fuelTankMax);
        fuelPump n2 = new fuelPump(0,fuelAmount,fuelTankMin,fuelTankMax);
        fuelPump n3 = new fuelPump(0,fuelAmount,fuelTankMin,fuelTankMax);
        fuelPump n4 = new fuelPump(0,fuelAmount,fuelTankMin,fuelTankMax);


        int choiceOfPump=0;

        do {

            if(choiceOfPump>4 && choiceOfPump<1){
                System.out.println("Invalid input (chose 1-4)");
            }

            Scanner fuelInput = new Scanner(System.in);
            System.out.println("Welcome to Riccardi´s gas Station, which pump do you want to use?");
            choiceOfPump = fuelInput.nextInt();


        } while(choiceOfPump>4 && choiceOfPump<1);

        switch (choiceOfPump){
            case 1:
                useFuelTank(n1, fuelTankMax, fuelTankMin,fuelAmount);
                break;
            case 2:
                useFuelTank(n2, fuelTankMax, fuelTankMin,fuelAmount);
                break;
            case 3:
                useFuelTank(n3, fuelTankMax, fuelTankMin,fuelAmount);
                break;
            case 4:
                useFuelTank(n4, fuelTankMax, fuelTankMin,fuelAmount);
                break;

        }



    }

    /**
     * This a gas station user experience process there is 4 fuel pumps that are connected with the gas tank and the client comes and
     * asks for gas and then that amount gets substacted from the gas tank and then the gas track comes and automatically refills the
     * gas tank.
     * @param number
     * @param fuelTankMax
     * @param fuelTankMin
     * @param fuelAmount
     */

    public static void useFuelTank(fuelPump number, int fuelTankMax, int fuelTankMin, int fuelAmount){
        String gasTruck = "Here the gas truck comes";

        Scanner fuelInput = new Scanner(System.in);
        System.out.println("How much liters you want?");
        int clientInputForGas = fuelInput.nextInt();
        System.out.println(gasTruck +( " wants to add ") + clientInputForGas + ( " liters of gas" ));
        int math = fuelTankMax - clientInputForGas;
        System.out.println(math + " liters left in the gas tank");
        int workerFillsTank = fuelInput.nextInt();
        if (fuelTankMax <= 0) {
            System.out.println("The gas tank is empty, please refill it to continue using the gas station" + fuelTankMax + "liters");

        }
    }
}



class fuelPump {

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public void addCounter(int counter) {
        this.counter = counter+this.counter;
    }

    int counter;

    public fuelPump(int counter, int fuelAmountInTank,int fuelTankMin, int fuelTankMax){
        this.counter = counter;
    }
}
